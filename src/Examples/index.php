<?php


include '../WireCardTRPHP/BaseModel.php';
include '../WireCardTRPHP/WDTicketPaymentFormRequest.php';
include '../WireCardTRPHP/TokenizeCCRequest.php';
include '../WireCardTRPHP/TokenizeCCURLRequest.php';
include '../WireCardTRPHP/Settings.php';
include '../WireCardTRPHP/restHttpCaller.php';



function main()
{
//    echo "STARTED ----- <br>";
//Request
    $settings = new Settings();
    $settings->UserCode = 0000;
    $settings->Pin = "123123213213213";
    $request = new WDTicketPaymentFormRequest();
    $request->ServiceType = "WDTicket";
    $request->OperationType = "Sale3DSURLProxy";
    $request->Token= new Token();
    $request->Token->UserCode=$settings->UserCode;
    $request->Token->Pin=$settings->Pin;
    $request->Price = 1;
    $request->MPAY = "";
    $request->CurrencyCode = "EUR";
    $request->ErrorURL = "http://localhost:8000/fail.php";
    $request->SuccessURL = "http://localhost:8000/success.php";
    $request->ExtraParam = "";
    $request->PaymentContent = "Bilgisayar";
    $request->Description = "BLGSYR01";
    $request->PaymentTypeId = 1;
    $request->InstallmentOptions = 0; //no Installment which is oneshot
    $request->CustomerInfo = new CustomerInfo();
    $request->CustomerInfo->CustomerName = "ahmet";
    $request->CustomerInfo->CustomerSurname = "yılmaz";
    $request->CustomerInfo->CustomerEmail = "ahmet.yilmaz@gmail.com";
    $request->Language = "TR";

    $request->BaseUrl =  $settings->BaseUrl;
    $response = "";

//    echo $request->toXmlString();
    $response = WDTicketPaymentFormRequest::execute($request);

//    header('Content-type: text/xml');

    $sxml = new SimpleXMLElement($response);
    $responseUrl=$sxml->Item[3]['Value'];

    $response = str_replace('&', '&amp;', $response);
    $response = str_replace('<', '&lt;', $response);
    echo '<pre>' . $response . '</pre>';
    echo "<br>";
    echo "<br>";
    echo "<br>";
    echo "THE RESPONSE URL: <br>";
    echo $responseUrl;
//    echo ($response);
//    echo $settings->BaseUrl;
//    echo "<br> END ------- \n";
    return $responseUrl;
}


main();