<?php


namespace WireCardManager\Components;


use WireCardManager\Enums\InstallmentOptions;
use WireCardManager\Enums\OperationTypes;

class ManagerData
{

    protected $currency;
    protected $language = 'EN';
    protected $buyerName;
    protected $buyerSurname;

    protected $buyerEmail;
    protected $price;
    protected $successUrl = "http://localhost:8000/success.php";
    protected $failUrl = "http://localhost:8000/fail.php";

    protected $paymentContent;
    protected $paymentDescription;

    protected $installmentOption = InstallmentOptions::_ONE_SHOT; //oneshot Installment

    protected $operationType = OperationTypes::_NORMAL;


    protected $paymentPageLanguage = 'DefLang=EN';


    public function getExtraParam()
    {

        $extraParams[] = $this->paymentPageLanguage;


        return call_user_func(function() use ($extraParams){
            $x = "";
            foreach ($extraParams as $s) $x.=$s;
            return $x;
        });
    }

    public function setPaymentPageLanguage($lang)
    {
        $this->paymentPageLanguage='DefLang='.$lang;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param mixed $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param string $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    /**
     * @return mixed
     */
    public function getBuyerName()
    {
        return $this->buyerName;
    }

    /**
     * @param mixed $buyerName
     */
    public function setBuyerName($buyerName)
    {
        $this->buyerName = $buyerName;
    }

    /**
     * @return mixed
     */
    public function getBuyerEmail()
    {
        return $this->buyerEmail;
    }

    /**
     * @param mixed $buyerEmail
     */
    public function setBuyerEmail($buyerEmail)
    {
        $this->buyerEmail = $buyerEmail;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getSuccessUrl()
    {
        return $this->successUrl;
    }

    /**
     * @param string $successUrl
     */
    public function setSuccessUrl($successUrl)
    {
        $this->successUrl = $successUrl;
    }

    /**
     * @return string
     */
    public function getFailUrl()
    {
        return $this->failUrl;
    }

    /**
     * @param string $failUrl
     */
    public function setFailUrl($failUrl)
    {
        $this->failUrl = $failUrl;
    }

    /**
     * @return mixed
     */
    public function getPaymentContent()
    {
        return $this->paymentContent;
    }

    /**
     * @param mixed $paymentContent
     */
    public function setPaymentContent($paymentContent)
    {
        $this->paymentContent = $paymentContent;
    }

    /**
     * @return mixed
     */
    public function getPaymentDescription()
    {
        return $this->paymentDescription;
    }

    /**
     * @param mixed $paymentDescription
     */
    public function setPaymentDescription($paymentDescription)
    {
        $this->paymentDescription = $paymentDescription;
    }

    /**
     * @return int
     */
    public function getInstallmentOption()
    {
        return $this->installmentOption;
    }

    /**
     * @param int $installmentOption
     */
    public function setInstallmentOption($installmentOption)
    {
        $this->installmentOption = $installmentOption;
    }

    /**
     * @return string
     */
    public function getOperationType()
    {
        return $this->operationType;
    }

    /**
     * @param string $operationType
     */
    public function setOperationType($operationType)
    {
        $this->operationType = $operationType;
    } // default


    public function getCustomerInfo()
    {
        $customerInfo = new \CustomerInfo();
        $customerInfo->CustomerName = $this->buyerName;
        $customerInfo->CustomerSurname = $this->buyerSurname;
        $customerInfo->CustomerEmail = $this->buyerEmail;
        return $customerInfo;
    }

    /**
     * @return mixed
     */
    public function getBuyerSurname()
    {
        return $this->buyerSurname;
    }

    /**
     * @param mixed $buyerSurname
     */
    public function setBuyerSurname($buyerSurname)
    {
        $this->buyerSurname = $buyerSurname;
    }







}