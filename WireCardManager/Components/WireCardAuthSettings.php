<?php


namespace WireCardManager\Components;


class WireCardAuthSettings extends \Settings
{

    /**
     *
     *  just an inheritance for future purposes, you may use instead of \Settings
     *
     */

    public $Environment = 'Production';

}