<?php


namespace WireCardManager\Components;


use WireCardManager\Results\WireCardPaymentResult;

class HashCalculator
{

    protected $settings;

    public function __construct(WireCardAuthSettings $settings)
    {
        $this->settings = $settings;
    }


    public function calculateHash(WireCardPaymentResult $result)
    {
        $hashtr = $result->statusCode .  $result->lastTransactionDate . $result->MPAY . strtolower($result->orderId) . $this->settings->HashKey;
        $hashbytes = mb_convert_encoding($hashtr, "ISO-8859-9");
        $hash = base64_encode ( sha1 ( $hashbytes, true ) );
        return $hash;
    }

    public function checkHashes($first,$second)
    {
        return ($first === $second);
    }

}