<?php


namespace WireCardManager\Components;


use WireCardManager\Enums\PaymentResultSuccess;
use WireCardManager\Results\WireCardPaymentResult;
use WireCardManager\Results\WireCardRedirectResult;

class ResultAcceptanceChecker
{

    public function check(WireCardRedirectResult $wireCardRedirectResult)
    {
        return ($wireCardRedirectResult->statusCode == PaymentResultSuccess::_SUCCESSFUL);
    }

    public function checkResult(WireCardPaymentResult $wireCardPaymentResult)
    {
        return ($wireCardPaymentResult->statusCode == PaymentResultSuccess::_SUCCESSFUL);
    }

}