<?php


namespace WireCardManager;


use WireCardManager\Components\ManagerData;
use WireCardManager\Components\ResultAcceptanceChecker;
use WireCardManager\Factories\WireCardRequestFactory;
use WireCardManager\Factories\WireCardSettingsFactory;
use WireCardManager\Results\WireCardRedirectResult;

class WireCardManager
{

    protected $managerData;
    protected $wireCardRequestFactory;
    protected $paymentResultChecker;
    protected $wireCardSettingsFactory;

    public function __construct(ManagerData $managerData)
    {
        $this->managerData = $managerData;
        $this->wireCardRequestFactory = new WireCardRequestFactory();
        $this->wireCardSettingsFactory = new WireCardSettingsFactory();
        $this->paymentResultChecker = new ResultAcceptanceChecker();
    }

    protected function makeWireCardRequest()
    {
        return $this->wireCardRequestFactory->makeWireCardRequest($this->wireCardSettingsFactory->make());
    }

    protected function buildWireCardRequest()
    {
        $wireCardRequest = $this->makeWireCardRequest();

        $wireCardRequest->OperationType = $this->managerData->getOperationType();
        $wireCardRequest->Price = $this->managerData->getPrice();
        $wireCardRequest->CurrencyCode = $this->managerData->getCurrency();
        $wireCardRequest->ErrorURL = $this->managerData->getFailUrl();
        $wireCardRequest->SuccessURL = $this->managerData->getSuccessUrl();
        $wireCardRequest->PaymentContent = $this->managerData->getPaymentContent();
        $wireCardRequest->Description = $this->managerData->getPaymentDescription();
        $wireCardRequest->InstallmentOptions = $this->managerData->getInstallmentOption();
        $wireCardRequest->CustomerInfo = $this->managerData->getCustomerInfo();
        $wireCardRequest->Language = $this->managerData->getLanguage();
        $wireCardRequest->ExtraParam = $this->managerData->getExtraParam();

        return $wireCardRequest;
    }

    public function pay()
    {
        $request = $this->buildWireCardRequest();
        $result = \WDTicketPaymentFormRequest::execute($request);

        $wireCardPaymentResult = new WireCardRedirectResult();
        $wireCardPaymentResult->setResult($result);
        return $wireCardPaymentResult;
    }

    /**
     * @return ManagerData
     */
    public function getManagerData()
    {
        return $this->managerData;
    }

    /**
     * @param ManagerData $managerData
     */
    public function setManagerData(ManagerData $managerData)
    {
        $this->managerData = $managerData;
    }

    /**
     * @return WireCardRequestFactory
     */
    public function getWireCardRequestFactory()
    {
        return $this->wireCardRequestFactory;
    }

    /**
     * @param WireCardRequestFactory $wireCardRequestFactory
     */
    public function setWireCardRequestFactory(WireCardRequestFactory $wireCardRequestFactory)
    {
        $this->wireCardRequestFactory = $wireCardRequestFactory;
    }


    /**
     * @return WireCardSettingsFactory
     */
    public function getWireCardSettingsFactory()
    {
        return $this->wireCardSettingsFactory;
    }

    /**
     * @param WireCardSettingsFactory $wireCardSettingsFactory
     */
    public function setWireCardSettingsFactory(WireCardSettingsFactory $wireCardSettingsFactory)
    {
        $this->wireCardSettingsFactory = $wireCardSettingsFactory;
    }


}