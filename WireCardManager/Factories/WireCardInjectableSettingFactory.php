<?php


namespace WireCardManager\Factories;


use WireCardManager\Components\WireCardAuthSettings;

class WireCardInjectableSettingFactory extends WireCardSettingsFactory
{

    public $pin;
    public $hash;
    public $userCode;

    public function make()
    {
        $authSettings = new WireCardAuthSettings();
        $authSettings->Pin = $this->pin;
        $authSettings->HashKey = $this->hash;
        $authSettings->UserCode = $this->userCode;
        return $authSettings;
    }

}
