<?php


namespace WireCardManager\Factories;


use WireCardManager\Components\WireCardAuthSettings;

class WireCardRequestFactory
{

    public function makeWireCardRequest(WireCardAuthSettings $settings)
    {
        return $this->makeBase($settings);
    }

    //only includes auth
    protected function makeBase(\Settings $settings)
    {
        $request = new \WDTicketPaymentFormRequest();
        $request->ServiceType = "WDTicket";
        $request->Token= new \Token();
        $request->Token->UserCode=$settings->UserCode;
        $request->Token->Pin=$settings->Pin;



        $request->OperationType = null;
//        $request->Price = 1;
        $request->MPAY = "";
//        $request->CurrencyCode = "EUR";
        $request->ErrorURL = "http://localhost:8000/fail.php";
        $request->SuccessURL = "http://localhost:8000/success.php";
        $request->ExtraParam = "";
        $request->PaymentContent = "Bilgisayar";
        $request->Description = "BLGSYR01";
        $request->PaymentTypeId = 1;
        $request->InstallmentOptions = 0; //no Installment which is oneshot
//        $request->CustomerInfo = new \CustomerInfo();
//        $request->CustomerInfo->CustomerName = "ahmet";
//        $request->CustomerInfo->CustomerSurname = "yılmaz";
//        $request->CustomerInfo->CustomerEmail = "ahmet.yilmaz@gmail.com";
//        $request->Language = 'EN';

        $request->BaseUrl =  $settings->BaseUrl;


        return $request;
    }

}