<?php


namespace WireCardManager\Factories\Development;


use WireCardManager\Components\WireCardAuthSettings;
use WireCardManager\Components\WireCardAuthSettingsTest;
use WireCardManager\Factories\WireCardSettingsFactory as BaseWireCardSettingsFactory;


class WireCardSettingsFactory extends BaseWireCardSettingsFactory
{
    public function make()
    {
        $wireCardAuthSettings = new WireCardAuthSettings();
        return $wireCardAuthSettings;
    }
}