<?php


namespace WireCardManager\Factories;


use WireCardManager\Components\ManagerData;

class ManagerDataFactory
{

    public function make()
    {
        $managerComponent = new ManagerData();
        $managerComponent->setBuyerName("_test_name");
        $managerComponent->setBuyerSurname("_test_surname");
        $managerComponent->setBuyerEmail("test@test.com");
        $managerComponent->setLanguage('EN');
        $managerComponent->setSuccessUrl("http://localhost:8000/success.php");
        $managerComponent->setFailUrl("http://localhost:8000/fail.php");
        $managerComponent->setPaymentContent("the thing i bought");
        $managerComponent->setPaymentDescription("description of the thing i bought");
        $managerComponent->setInstallmentOption(\WireCardManager\Enums\InstallmentOptions::_ONE_SHOT);
        $managerComponent->setOperationType(\WireCardManager\Enums\OperationTypes::_3D);
        $managerComponent->setPrice(1);
        $managerComponent->setCurrency('EUR');
        $managerComponent->setPaymentPageLanguage('EN');
        return $managerComponent;
    }

}