<?php


namespace WireCardManager\Factories;


use WireCardManager\Components\WireCardAuthSettings;

class WireCardSettingsFactory
{
    public function make()
    {
        $wireCardAuthSettings = new WireCardAuthSettings();
        return $wireCardAuthSettings;
    }
}