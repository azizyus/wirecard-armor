<?php


namespace WireCardManager\Results;


class WireCardRedirectResult
{

    public $statusCode;
    public $resultCode;
    public $resultMessage;
    public $redirectUrl;

    public function setResult($response)
    {
        $sxml = new \SimpleXMLElement($response);

        $this->statusCode=(String)$sxml->Item[0]['Value'];
        $this->resultCode=(String)$sxml->Item[1]['Value'];
        $this->resultMessage=(String)$sxml->Item[2]['Value'];
        $this->redirectUrl=(String)$sxml->Item[3]['Value'];
    }

}
