<?php


namespace WireCardManager\Results;


class WireCardPaymentResult
{

    public $orderId;
    public $MPAY;
    public $statusCode;
    public $resultCode;
    public $resultMessage;
    public $lastTransactionDate;
    public $maskedCCNo;
    public $CCTokenId;
    public $extraParam;
    public $hashParam;

    public function catchRequest()
    {
        $this->orderId = $_POST['OrderId'];
        $this->MPAY = $_POST['MPAY'];
        $this->statusCode = $_POST['StatusCode'];
        $this->resultCode = $_POST['ResultCode'];
        $this->resultMessage = $_POST['ResultMessage'];
        $this->lastTransactionDate = $_POST['LastTransactionDate'] ?? '_NOT_IN_REQUEST';
        $this->maskedCCNo = $_POST['MaskedCCNo'] ?? '_NOT_IN_REQUEST';
        $this->CCTokenId = $_POST['CCTokenId'] ?? '_NOT_IN_REQUEST';
        $this->extraParam = $_POST['ExtraParam'];
        $this->hashParam = $_POST['HashParam'] ?? '_NOT_IN_REQUEST'; //the only parameter which may not exist in success/fail result
    }


}