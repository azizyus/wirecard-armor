<?php


namespace WireCardManager\Enums;


class Currency
{


    const _TURKISH_LIRA = "TRY";
    const _US_DOLLAR = "USD";
    const _EURO = "EURO";


    public function mapCurrency($currencyCode,$default=null)
    {
        $map = [
            949 =>'TRY',
            978 =>'EUR',
            840 =>'USD'
        ];
        return $map[$currencyCode]??$default;
    }

}