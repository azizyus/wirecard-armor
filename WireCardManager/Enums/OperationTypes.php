<?php


namespace WireCardManager\Enums;


class OperationTypes
{

    const _3D = 'Sale3DSURLProxy';
    const _NORMAL = 'SaleURLProxy';

}