<?php


namespace WireCardManager\Enums;


class PaymentResultSuccess
{

    const _SUCCESSFUL = 0;
    const _FAILED = 1;

}